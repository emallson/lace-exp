import luigi
import numpy as np
from .greedy import InfBase as Greedy, threshold, fast, standard, Brute
from .mixins import DictOutputMixin
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats
import pandas as pd
import seaborn as sns
import subprocess
import json

ALGS = {
    "threshold": threshold,
    "fast": fast,
    "standard": standard,
}

PRETTY = {
    "threshold": "Threshold",
    "fast": "Fast",
    "standard": "Standard",
    ("d", "fast"): r"$\gamma_d$",
    ("s", "fast"): r"FastGreedy SM Ratio $\gamma_s$",
    ("beta", "fast"): r"FastGreedy DR Ratio $\beta^*$",
    ("s", "threshold"): r"Thresh $\gamma_s$",
    ("s", "standard"): r"$\gamma_s$, Standard",
    ("d", "threshold"): r"Thresh $\gamma_d$",
    10: "10 levels",
    100: "100 levels",
}

COLORS = {
    "threshold": "b",
    "fast": "g",
    "standard": "r",
    ("d", "fast"): "g",
    ("s", "fast"): "k",
    ("beta", "fast"): "blue",
    ("s", "threshold"): "red",
    ("s", "standard"): "brown",
    ("beta", "threshold"): "orange",
    ("d", "threshold"): "g",
    10: "k",
    100: "g",
}

MARKERS = {
    "threshold": "+",
    "fast": "*",
    "standard": "^",
    ("d", "fast"): "*",
    ("s", "fast"): "^",
    ("beta", "fast"): "+",
    ("s", "threshold"): "v",
    ("s", "standard"): "s",
    ("beta", "threshold"): "o",
    ("d", "threshold"): 'x',
    10: "+",
    100: "^",
}

STYLES = {
    1: ":",
    10: None,
    100: "--",
}

class PerformanceComparisonFigures(luigi.WrapperTask):
    threads = luigi.IntParameter(default=10)
    repetitions = luigi.IntParameter(default=100)

    def requires(self):
        return [
            # GrQc
            PerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=1),
            RuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=1),
            PerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions),
            RuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions),
            EvalsFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions),
            PerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"]),
            RuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"]),
            EvalsFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"]),
            # facebook
            PerformanceFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, steps=1, algs=["threshold", "fast"]),
            RuntimeFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, steps=1, algs=["threshold", "fast"]),
            PerformanceFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, algs=["threshold", "fast"]),
            RuntimeFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, algs=["threshold", "fast"]),
            EvalsFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, algs=["threshold", "fast"]),
            PerformanceFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"], ks=[20,30,40,50]),
            RuntimeFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"], ks=[20,30,40,50]),
            EvalsFig(dataset="facebook_uniform", threads=self.threads, repetitions=self.repetitions, steps=100, algs=["threshold", "fast"], ks=[20,30,40,50]),
            # tossing some parameter plots in because why not
            ParameterPerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="epsilon", values=[0.8, 0.4, 0.2, 0.1, 0.05, 0.02], delta=0.9, kappa=0.95),
            ParameterPerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="delta", values=[0.95, 0.9, 0.8, 0.6, 0.2], epsilon=0.05, kappa=0.95),
            ParameterPerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="kappa", values=[0.95, 0.9, 0.8, 0.6, 0.2], epsilon=0.05, delta=0.9),
            ParameterRuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="epsilon", values=[0.8, 0.4, 0.2, 0.1, 0.05, 0.02], delta=0.9, kappa=0.95),
            ParameterRuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="delta", values=[0.95, 0.9, 0.8, 0.6, 0.2], epsilon=0.05, kappa=0.95),
            ParameterRuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, varying="kappa", values=[0.95, 0.9, 0.8, 0.6, 0.2], epsilon=0.05, delta=0.9),
            ParameterGridPerformanceFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, epsilon=0.05, alg="fast",
                                        kappa_values=np.arange(0.95, 0, -0.1),
                                        delta_values=np.arange(0.95, 0, -0.1)),
            ParameterGridRuntimeFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=10, epsilon=0.05, alg="fast",
                                        kappa_values=np.arange(0.95, 0, -0.1),
                                        delta_values=np.arange(0.95, 0, -0.1)),
            BetaFig(dataset="ca-GrQc_uniform", threads=self.threads, repetitions=self.repetitions, steps=[10,100]),
            BruteGammaFigs(threads=self.threads, repetitions=self.repetitions),
        ]

class LinePlotMixin:
    def write_fig(self, data, plotby, iterateby="alg", groupby="k", path=None, xlabel="$K$ -- Maximum \\# of deterministic seeds", ylabel=None, ylim=None, yticker=None, ownfig=True, legend=True):
        if path is None:
            path = self.output().path
        if ownfig:
            fig = plt.figure(figsize=(4,3))
        lines = []
        for alg, df in data.groupby(iterateby):
            mean = df.groupby(groupby)[plotby].mean()
            xs = mean.index.get_level_values(groupby)
            #  below, above = stats.t.interval(0.95, self.repetitions - 1, loc=mean, scale = df.groupby(groupby)[plotby].std())
            std = df.groupby(groupby)[plotby].std()
            below = mean - std / 2
            above = mean + std / 2
            lines.append(plt.plot(xs, mean, label=PRETTY[alg], color=COLORS[alg], marker=MARKERS[alg])[0])
            plt.fill_between(xs, below, above, alpha=0.2, color=COLORS[alg], antialiased=True)
        if legend:
            plt.legend()
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if ylim is not None:
            plt.ylim(ylim)

        if yticker is not None:
            from matplotlib.ticker import FuncFormatter
            plt.gca().get_yaxis().set_major_formatter(FuncFormatter(yticker))
        plt.grid(color='#222222', linestyle='--', alpha=0.2)
        if ownfig:
            plt.savefig(path, bbox_inches="tight")
            plt.close()
        return lines

class PerformanceFig(DictOutputMixin, LinePlotMixin, luigi.Task):
    LOG_PATH = "figs/perf/{}.pdf"
    dataset = luigi.Parameter()
    threads = luigi.IntParameter(significant=False)
    algs = luigi.ListParameter(default=["standard", "threshold", "fast"])
    #  ks = luigi.ListParameter(default=[1,5,10])
    ks = luigi.ListParameter(default=[20,40,60,80,100])
    steps = luigi.IntParameter(default=10)
    repetitions = luigi.IntParameter(default=10)

    # algorithm params
    epsilon = luigi.FloatParameter(default=0.05)
    delta = luigi.FloatParameter(default=0.9)
    kappa = luigi.FloatParameter(default=0.95)

    def requires(self):
        reqs = {
            alg: {
                k: [ALGS[alg](rep, self.dataset, k, self.epsilon, self.delta, self.kappa, self.steps, threads=self.threads) for rep in range(self.repetitions)]
                for k in self.ks
            }
            for alg in self.algs
        }
        return reqs

    def df(self):
        records = []
        for alg, data in self.requires().items():
            for k, tasks in data.items():
                for rep, task in enumerate(tasks):
                    records += [{"alg": alg, "k": k, "rep": rep, "runtime": task.runtime(), "walltime": task.wallclock(), "value": task.solution_value(), "evals": task.function_evals()}]
        df = pd.DataFrame(records)
        return df

    def run(self):
        self.output().makedirs()
        df = self.df()
        means = df.groupby(["alg", "k"]).mean()
        print("Lost up to a factor of: {}".format(1 - (means.ix["fast", :]["value"] / means.ix["threshold", :]["value"]).min()))
        self.write_fig(df, plotby='value', ylabel="Solution Value")

class RuntimeFig(PerformanceFig):
    LOG_PATH = "figs/runtime/{}.pdf"

    def run(self):
        self.output().makedirs()
        df = self.df()
        fast_df = df[df["alg"] == "fast"]
        fast_mean = fast_df.groupby('k')["runtime"].mean()
        for alg, df in df.groupby('alg'):
            if alg == "fast":
                continue
            means = df.groupby('k')["runtime"].mean()

            print("{}: {}".format(alg, (means / fast_mean).max()))

        self.write_fig(self.df(), plotby='runtime', ylabel="Running Time (Thousands of sec.)", yticker=lambda tick, pos: "{}".format(int(tick / 1000)))

class EvalsFig(PerformanceFig):
    LOG_PATH = "figs/evals/{}.pdf"
    normalize = luigi.BoolParameter(default=False)

    def run(self):
        self.output().makedirs()
        df = self.df()
        if self.normalize:
            n = json.loads(subprocess.run(['describe-graph', Greedy.graph_path(self.dataset), '--json'], stdout=subprocess.PIPE, check=True).stdout)["nodes"]
            norm = n * df["k"] * self.steps
            print(norm)
            df["evals"] = df["evals"] / norm

        means = df.groupby(["alg", "k"]).mean()
        print("Gained at least a factor of (evals): {}".format(1 - (means.ix["fast", :]["evals"] / means.ix["threshold", :]["evals"]).max()))
        print("Gained at most a factor of (evals): {}".format(1 - (means.ix["fast", :]["evals"] / means.ix["threshold", :]["evals"]).min()))
        
        if "standard" in self.algs:
            fig = plt.figure(figsize=(4,3))
            lines = self.write_fig(df[df["alg"] != "standard"], 'evals', ylabel="Function Queries (Thousands)", yticker=lambda tick, pos: "{}".format(int(tick / 1000)), ownfig=False, legend=False)

            ax = plt.axes([.65, .2, .2, .2])
            lines += self.write_fig(df[df["alg"] == 'standard'], 'evals', ylabel="", xlabel="", legend=False, yticker=lambda tick, pos: "{}".format(int(tick / 1000)), ownfig=False)
            fig.legend(tuple(lines), ("Fast", "Threshold", "Standard"), loc='upper left', bbox_to_anchor=(0.18, 0.94))
            plt.savefig(self.output().path, bbox_inches='tight')
            plt.close()
        else:
            self.write_fig(df, 'evals', ylabel="Function Queries (Thousands)", yticker=lambda tick, pos: "{}".format(int(tick / 1000)))

class ParameterPerformanceFig(DictOutputMixin, LinePlotMixin, luigi.Task):
    LOG_PATH = "figs/perf/{}.pdf"
    dataset = luigi.Parameter()
    threads = luigi.IntParameter(significant=False)
    algs = luigi.ListParameter(default=["threshold", "fast"])
    k = luigi.IntParameter(default=20)
    steps = luigi.IntParameter(default=10)
    repetitions = luigi.IntParameter(default=10)

    # algorithm params
    varying = luigi.ChoiceParameter(choices=["epsilon", "delta", "kappa"])
    values = luigi.ListParameter()

    epsilon = luigi.FloatParameter(default=None)
    delta = luigi.FloatParameter(default=None)
    kappa = luigi.FloatParameter(default=None)

    def xlabel(self):
        if self.varying == "epsilon":
            return "$\epsilon$"
        elif self.varying == "delta":
            return "$\delta$"
        elif self.varying == "kappa":
            return "$\kappa$"

    def requires(self):
        param = lambda base, val: base if base is not None else val
        reqs = {
            alg: {
                val: [ALGS[alg](rep, self.dataset, self.k, param(self.epsilon, val), param(self.delta, val), param(self.kappa, val), self.steps, threads=self.threads) for rep in range(self.repetitions)]
                for val in self.values
            }
            for alg in self.algs
        }
        return reqs

    def df(self):
        records = []
        for alg, data in self.requires().items():
            for val, tasks in data.items():
                for rep, task in enumerate(tasks):
                    records += [{"alg": alg, self.varying: val, "k": self.k, "rep": rep, "runtime": task.runtime(), "walltime": task.wallclock(), "value": task.solution_value()}]
        df = pd.DataFrame(records)
        return df

    PLOT_KEY = "value"
    YLABEL = "Solution Value"
    def run(self):
        self.output().makedirs()
        df = self.df()
        self.write_fig(df, plotby=self.PLOT_KEY, groupby=self.varying,
                       xlabel=self.xlabel(),
                       ylabel=self.YLABEL)

class ParameterRuntimeFig(ParameterPerformanceFig):
    LOG_PATH = "figs/runtime/{}.pdf"
    PLOT_KEY = "runtime"
    YLABEL = "Running Time (s)"

class BetaFig(PerformanceFig):
    LOG_PATH = "figs/beta/{}.pdf"
    algs = ["fast"] # others don't have gamma
    steps = luigi.ListParameter(default=[10,100])

    def requires(self):
        reqs = {
            steps: {
                k: [ALGS["fast"](rep, self.dataset, k, self.epsilon, self.delta, self.kappa, steps, threads=self.threads) for rep in range(self.repetitions)]
                for k in self.ks
            }
            for steps in self.steps
        }
        return reqs

    def df(self):
        records = []
        for steps, data in self.requires().items():
            for k, tasks in data.items():
                for rep, task in enumerate(tasks):
                    records += [{"steps": steps, "alg": "fast", "k": k, "rep": rep, "runtime": task.runtime(), "walltime": task.wallclock(), "value": task.solution_value(), "beta": task.beta()}]
        df = pd.DataFrame(records)
        return df

    def run(self):
        self.output().makedirs()
        df = self.df()

        fig = plt.figure(figsize=(4,3))
        self.write_fig(df, "beta", iterateby="steps", ylabel=r"$\beta^*$", ylim=[0.6, 0.9], ownfig=False)
        plt.gca().set_ylabel(r"$\beta^*$", fontsize=20)
        plt.savefig(self.output().path, bbox_inches='tight')
        plt.close()

class ParameterGridPerformanceFig(DictOutputMixin, luigi.Task):
    LOG_PATH = "figs/perf/{}.pdf"
    dataset = luigi.Parameter()
    threads = luigi.IntParameter(significant=False)
    alg = luigi.ChoiceParameter(choices=['fast', 'threshold'])
    k = luigi.IntParameter(default=20)
    steps = luigi.IntParameter(default=10)
    repetitions = luigi.IntParameter(default=10)

    # algorithm params
    epsilon = luigi.FloatParameter(default=0.05)
    kappa_values = luigi.ListParameter(default=[0.95, 0.9, 0.8, 0.6, 0.2])
    delta_values = luigi.ListParameter(default=[0.95, 0.9, 0.8, 0.6, 0.2])

    def requires(self):
        reqs = {
            self.alg: {
                (kappa, delta): [ALGS[self.alg](rep, self.dataset, self.k, self.epsilon, delta, kappa, self.steps, threads=self.threads) for rep in range(self.repetitions)]
                for kappa in self.kappa_values
                for delta in self.delta_values
            }
        }
        return reqs

    ZFIELD = "value"
    ZLABEL = r'Solution Value'
    def run(self):
        vals = []
        for (kappa, delta), tasks in self.requires()[self.alg].items():
            for task in tasks:
                vals += [{"kappa": kappa, "delta": delta, "value": task.solution_value(), "runtime": task.runtime()}]
        df = pd.DataFrame(vals)
        means = df.groupby(["kappa", "delta"]).mean().reset_index()
        means = means.pivot(index='kappa', columns='delta', values=self.ZFIELD)
        means = means.reindex(index=means.index[::-1])
        print(means)

        fig = plt.figure()
        sns.heatmap(means)
        plt.xlabel(r'$\delta$')
        plt.ylabel(r'$\kappa$')
        plt.savefig(self.output().path, bbox_inches='tight')
        plt.close()

class ParameterGridRuntimeFig(ParameterGridPerformanceFig):
    LOG_PATH = "figs/runtime/{}.pdf"
    ZFIELD = "runtime"
    ZLABEL = "Running Time (s)"

class BruteGammaFigs(DictOutputMixin, LinePlotMixin, luigi.Task):
    LOG_PATH = "figs/brute/{}.pdf"
    ks = luigi.ListParameter(default=[5,7,9,10])
    repetitions = luigi.IntParameter(default=10)
    threads = luigi.IntParameter(significant=False)
    steps = luigi.IntParameter(default=10)
    samples = luigi.IntParameter(default=None)

    # algorithm params
    epsilon = luigi.FloatParameter(default=0.05)
    delta = luigi.FloatParameter(default=0.9)
    kappa = luigi.FloatParameter(default=0.95)

    def requires(self):
        return {
            (gamma, alg): {
                k: [Brute(key=str(rep), graph="random-tiny", gamma=gamma, k=k, epsilon=self.epsilon, delta=self.delta, kappa=self.kappa, det_steps=self.steps, threads=self.threads, greedy=alg, samples=self.samples) # greedy doesn't actually matter for gamma-d, but matters for gamma-s
                    for rep in range(self.repetitions)]
                for k in self.ks
            }
            for gamma, alg in [("s", "fast")]
        }

    def df(self):
        records = []
        for (gamma, alg), data in self.requires().items():
            for k, tasks in data.items():
                for rep, task in enumerate(tasks):
                    records += [{"sense": gamma, "k": k, "rep": rep, "runtime": task.runtime(), "value": task.result_gamma(), "alg": alg}]
                    if gamma == 's' and alg == 'fast':
                        records += [{"sense": "beta", "k": k, "rep": rep, "runtime": task.runtime(), "value": task.beta(), "alg": alg}]
        df = pd.DataFrame(records)
        return df

    def run(self):
        self.output().makedirs()
        df = self.df()
        print("β: {}, γ_s: {}".format(df[df["sense"] == "beta"]["value"].min(), df[df["sense"] == "s"]["value"].min()))
        fig = plt.figure(figsize=(4,3))
        self.write_fig(df, "value", xlabel=r"$k$", ylabel="", ylim=[0,1.05], iterateby=["sense", "alg"], ownfig=False, legend=False)
        plt.legend()
        plt.savefig(self.output().path, bbox_inches='tight')
        plt.close()

class Everything(luigi.WrapperTask):
    threads = luigi.IntParameter(default=10)

    def requires(self):
        return PerformanceComparisonFigures(threads=self.threads, repetitions=10)
