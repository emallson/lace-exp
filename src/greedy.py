import luigi
import jsonlines
from luigi.contrib.external_program import ExternalProgramTask
from .mixins import ThreadsMixin, HashOutputMixin, TempPathMixin, RustBacktraceMixin, TimedMixin

def read_value(log, key, field):
    with jsonlines.open(log) as file:
        for row in file:
            if row["msg"] == key:
                return row[field]

DEFAULT_DET_STEPS = 10

class InfBase(TimedMixin, RustBacktraceMixin, ThreadsMixin, TempPathMixin, HashOutputMixin, ExternalProgramTask):
    graph = luigi.Parameter()
    k = luigi.IntParameter()
    threads = luigi.IntParameter(default=1, significant=False)
    box = luigi.IntParameter(default=None) # None => omit parameter from command
    key = luigi.Parameter()

    # inf params
    samples = luigi.IntParameter(default=None)
    det_steps = luigi.IntParameter(default=None)

    def auto_flags(self, skip=["graph", "k", "key"]):
        """ Automagically generate the flags for this by processing the parameters of the task."""
        return [v for key, val in self.param_kwargs.items()
                  if key not in skip
                  if val is not None
                  for v in ["--{}".format(key.replace("_", "-")), val]]

    @staticmethod
    def graph_path(g):
        return "data/graphs/{}.bin".format(g)

    def solution_value(self):
        if not self.complete():
            raise Exception("Cannot read solution value from incomplete task.")
        return read_value(self.output()["log"].path, "found solution", "value")

    def function_evals(self):
        if not super().complete():
            raise Exception("Cannot read function evals from incomplete task.")
        try:
            return read_value(self.output()["log"].path, "function evaluations", "delta") # benefit is never called except at the end to compute the final value -- not even gonna count it
        except TypeError:
            return None

    def sol_size(self):
        """ Returns the solution size (k as referred to in pseudocode).
        The `k` parameter of this method is actually the number of nodes
        we can deterministically seed. """
        if self.det_steps is not None:
            return self.k * self.det_steps
        else:
            return self.k * DEFAULT_DET_STEPS

    def run(self):
        self.output()["log"].makedirs()
        self.output()["time"].makedirs()
        
        super().run()


class Fast(InfBase):
    LOG_PATH = "logs/fast/{}.log"
    TIMER_PATH = "logs/runtimes/fast/{}.log"
    TMP_KEY = "log"

    epsilon = luigi.FloatParameter()
    delta = luigi.FloatParameter()
    kappa = luigi.FloatParameter()

    def program_args(self):
        return self.timed([
            "./impl/lace/target/release/inf",
            self.graph_path(self.graph),
            self.sol_size(),
            "--log",
            self._tmp_output,
            "--greedy",
            "fast",
        ] + self.auto_flags())

    def beta_seq(self):
        if not self.complete():
            raise Exception("Cannot read β from incomplete task.")
        betas = []
        with jsonlines.open(self.output()["log"].path) as file:
            for obj in file:
                if obj["msg"] == "iteration complete":
                    betas += [obj["β"]]
        return betas

    def beta(self):
        if not self.complete():
            raise Exception("Cannot read β from incomplete task.")
        return read_value(self.output()["log"].path, "fast greedy complete", "β")

class Brute(Fast):
    LOG_PATH = "logs/brute/{}.log"
    TIMER_PATH = "logs/runtimes/brute/{}.log"
    TMP_KEY = "log"

    gamma = luigi.ChoiceParameter(choices=['d', 's'])
    greedy = luigi.ChoiceParameter(choices=["fast", "threshold", "standard"], default="fast")

    def program_args(self):
        return self.timed([
            "./impl/lace/target/release/inf",
            "gamma-brute" if self.gamma == 'd' else "gamma-s-brute",
            self.graph_path(self.graph),
            self.k, # we use k directly for these
            "--log",
            self._tmp_output,
        ] + self.auto_flags(skip=['graph', 'k', 'key', 'gamma']))

    def result_gamma(self):
        if not self.complete():
            raise Exception("Cannot read γ from incomplete task.")
        if self.gamma == 's':
            return read_value(self.output()["log"].path, "brute forced greedy γ_s", "γ_s")
        elif self.gamma == 'd':
            return read_value(self.output()["log"].path, "brute forced greedy γ_d", "γ_d")


class Threshold(InfBase):
    LOG_PATH = "logs/threshold/{}.log"
    TIMER_PATH = "logs/runtimes/threshold/{}.log"
    TMP_KEY = "log"

    epsilon = luigi.FloatParameter()
    kappa = luigi.FloatParameter()

    def program_args(self):
        return self.timed([
            "./impl/lace/target/release/inf",
            self.graph_path(self.graph),
            self.sol_size(),
            "--log",
            self._tmp_output,
            "--greedy",
            "threshold",
        ] + self.auto_flags())

class Standard(InfBase):
    LOG_PATH = "logs/standard/{}.log"
    TIMER_PATH = "logs/runtimes/standard/{}.log"
    TMP_KEY = "log"

    def program_args(self):
        return self.timed([
            "./impl/lace/target/release/inf",
            self.graph_path(self.graph),
            self.sol_size(),
            "--log",
            self._tmp_output,
            "--greedy",
            "standard",
        ] + self.auto_flags())

def threshold(rep, graph, k, eps, _delta, kappa, steps, **kwargs):
    return Threshold(key=str(rep), graph=graph, k=k, epsilon=eps, kappa=kappa, det_steps=steps, **kwargs)

def fast(rep, graph, k, eps, delta, kappa, steps, **kwargs):
    return Fast(key=str(rep), graph=graph, k=k, epsilon=eps, delta=delta, kappa=kappa, det_steps=steps, **kwargs)

def standard(rep, graph, k, _eps, _delta, _kappa, steps, **kwargs):
    return Standard(key=str(rep), graph=graph, k=k, det_steps=steps, **kwargs)
